package main

import (
	disCache "distributed-cache/pkg/src"
	"flag"
	"fmt"
	"log"
	"net/http"
)

// go build -o server  -port=8001 &  -port=8002 &  -port=8003 -api=1 &
var db = map[string]string{
	"Tom":  "630",
	"Jack": "589",
	"Sam":  "567",
}

func createGroup() *disCache.Group {
	return disCache.NewGroup("scores", 2<<3, disCache.GetterFunc(
		func(key string) (string, error) {
			log.Println("[SlowDB] search key", key)
			if v, ok := db[key]; ok {
				return v, nil
			}
			return "", fmt.Errorf("%s not exist", key)
		}))
}

func startCacheServer(addr string, addrs []string, gee *disCache.Group) {
	httpPool := disCache.NewHttpPool(addr)
	httpPool.Set(addrs...)
	gee.RegisterPeers(httpPool)
	log.Println("discache is running at", addr)
	fmt.Println(addr)
	log.Fatal(http.ListenAndServe(addr[7:], httpPool))
}

func startAPIServer(apiAddr string, gee *disCache.Group) {
	http.Handle("/api", http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			key := r.URL.Query().Get("key")
			view, err := gee.Get(key)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.Header().Set("Content-Type", "application/octet-stream")
			w.Write([]byte(view))

		}))
	log.Println("fontend server is running at", apiAddr)
	log.Fatal(http.ListenAndServe(apiAddr[7:], nil))

}

func main() {
	var port int
	var api bool
	flag.IntVar(&port, "port", 8001, "Geecache server port")
	flag.BoolVar(&api, "api", false, "Start a api server?")
	flag.Parse()

	apiAddr := "http://localhost:9999"
	addrMap := map[int]string{
		8001: "http://localhost:8001",
		8002: "http://localhost:8002",
		8003: "http://localhost:8003",
	}

	var addrs []string
	for _, v := range addrMap {
		addrs = append(addrs, v)
	}

	gee := createGroup()
	if api {
		go startAPIServer(apiAddr, gee)
	}
	startCacheServer(addrMap[port], addrs, gee)
}
