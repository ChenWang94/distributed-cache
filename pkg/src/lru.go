package disCache

type Node struct {
	pre   *Node
	next  *Node
	key   string
	value string
}

type CacheLRU struct {
	maxCapacity     int8
	currentCapacity int8
	head            *Node
	tail            *Node
	cacheMap        map[string]*Node
}

func (cache *CacheLRU) Add(key, value string) {
	res := cache.cacheMap[key]
	if res != nil {
		res.value = value
		cache.Delete(key)
	} else {
		res = &Node{key: key, value: value}
		if cache.currentCapacity == cache.maxCapacity {
			cache.Delete(cache.tail.pre.key)
		}
	}

	cache.head.next.pre, res.next = res, cache.head.next
	cache.head.next, res.pre = res, cache.head
	cache.cacheMap[key] = res
	cache.currentCapacity++
}

func (cache *CacheLRU) Delete(key string) {
	if res := cache.cacheMap[key]; res != nil {
		res.next.pre, res.pre.next = res.pre, res.next
		cache.currentCapacity--
		delete(cache.cacheMap, key)
	}
}

func (cache *CacheLRU) Get(key string) *string {
	if res := cache.cacheMap[key]; res != nil {
		cache.Delete(key)
		cache.Add(key, res.value)
		return &res.value
	}
	return nil
}

func (cache *CacheLRU) Print() []string {
	view := cache.head
	res := make([]string, 0)
	for view != cache.tail {
		res = append(res, view.next.value)
		view = view.next
	}
	return res
}

func NewCacheLRU(maxCapacity int8) *CacheLRU {
	head := &Node{}
	tail := &Node{}
	head.next, tail.pre = tail, head
	return &CacheLRU{
		head:        head,
		tail:        tail,
		maxCapacity: maxCapacity,
		cacheMap:    make(map[string]*Node),
	}
}
