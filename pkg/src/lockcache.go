package disCache

import (
	"sync"
)

type LockCache struct {
	mu  sync.Mutex
	lru *CacheLRU
}

func (c *LockCache) add(key, value string) {
	c.mu.Lock()
	defer c.mu.Unlock()

	if c.lru == nil {
		c.lru = NewCacheLRU(100)
	}
	c.lru.Add(key, value)
}

func (c *LockCache) get(key string) *string {
	c.mu.Lock()
	defer c.mu.Unlock()

	return c.lru.Get(key)
}

func (c *LockCache) delete(key string) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.lru.Delete(key)
}
