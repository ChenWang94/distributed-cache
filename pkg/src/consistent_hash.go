package disCache

import (
	"fmt"
	"hash/crc32"
	"sort"
	"strconv"
)

// Hash maps bytes to uint32
type Hash func(data []byte) uint32

// Map contains all hashed keys
type Map struct {
	HashFunc Hash
	Replicas int
	Keys     []int // Sorted
	HashMap  map[int]string
}

// NewMap creates a Map instance
func NewMap(replicas int, fn Hash) *Map {
	m := &Map{
		Replicas: replicas,
		HashFunc: fn,
		HashMap:  make(map[int]string),
	}
	if m.HashFunc == nil {
		m.HashFunc = crc32.ChecksumIEEE
	}
	return m
}

// Add adds some keys to the hash.
func (m *Map) Add(keys ...string) {
	for _, key := range keys {
		for i := 0; i < m.Replicas; i++ {
			hash := int(m.HashFunc([]byte(strconv.Itoa(i) + key)))
			m.Keys = append(m.Keys, hash)
			m.HashMap[hash] = key
		}
	}
	sort.Ints(m.Keys)
	fmt.Println(m.Keys)
	fmt.Println(m.HashMap)
}

// Get gets the closest item in the hash to the provided key.
func (m *Map) Get(key string) string {
	if len(m.Keys) == 0 {
		return ""
	}

	hash := int(m.HashFunc([]byte(key)))
	// Binary search for appropriate replica.
	idx := sort.Search(len(m.Keys), func(i int) bool {
		return m.Keys[i] >= hash
	})

	return m.HashMap[m.Keys[idx%len(m.Keys)]]
}
