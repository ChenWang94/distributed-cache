package disCache

import (
	"fmt"
	"log"
	"sync"
)

type Getter interface {
	Get(key string) (string, error)
}

type GetterFunc func(key string) (string, error)

func (f GetterFunc) Get(key string) (string, error) {
	return f(key)
}

type Group struct {
	name      string
	getter    Getter
	mainCache LockCache

	peers PeerPicker
}

var (
	mu     sync.RWMutex
	groups = make(map[string]*Group)
)

func NewGroup(name string, maxCapacity int8, getter Getter) *Group {
	if getter == nil {
		panic("Getter is nil")
	}

	mu.Lock()
	defer mu.Unlock()

	g := &Group{
		name:      name,
		getter:    getter,
		mainCache: LockCache{lru: NewCacheLRU(maxCapacity)},
	}

	groups[name] = g
	return g
}

func GetGroup(name string) *Group {
	mu.RLock()
	defer mu.RUnlock()

	g := groups[name]
	return g
}

func (g *Group) Get(key string) (string, error) {
	if key == "" {
		return "", fmt.Errorf("key is required")
	}

	if res := g.mainCache.get(key); res != nil {
		log.Println("[disCache hit]")
		return *res, nil
	}

	return g.load(key)
}

func (g *Group) load(key string) (string, error) {
	if g.peers != nil {
		if peer, ok := g.peers.PickPeer(key); ok {
			if value, err := g.getFromPeer(peer, key); err == nil {
				return string(value), nil
			}
		}
	}

	return g.getLocally(key)
}

func (g *Group) getFromPeer(peer PeerGetter, key string) ([]byte, error) {
	bytes, err := peer.Get(g.name, key)
	if err != nil {
		return nil, err
	}
	return bytes, nil
}

func (g *Group) getLocally(key string) (string, error) {
	value, err := g.getter.Get(key)
	if err != nil {
		return "", err
	}
	g.populateCache(key, value)
	return value, nil
}

func (g *Group) populateCache(key, value string) {
	g.mainCache.add(key, value)
}

// RegisterPeers registers a PeerPicker for choosing remote peer
func (g *Group) RegisterPeers(peers PeerPicker) {
	if g.peers != nil {
		panic("RegisterPeerPicker called more than once")
	}
	g.peers = peers
}
