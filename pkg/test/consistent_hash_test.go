package test

import (
	"distributed-cache/pkg/src"
	"fmt"
	"strconv"
	"testing"
)

func TestHashing(t *testing.T) {
	hash := disCache.NewMap(3, func(key []byte) uint32 {
		i, _ := strconv.Atoi(string(key))
		return uint32(i)
	})

	// Given the above hash function, this will give replicas with "hashes":
	// 1, 3, 5, 11, 13, 15, 21, 23, 25
	hash.Add("1", "3", "5")

	fmt.Println(hash.HashMap)
	fmt.Println(hash.Keys)

	testCases := map[string]string{
		"2":  "3",
		"11": "1",
		"23": "3",
		"27": "1",
	}

	for k, v := range testCases {
		if hash.Get(k) != v {
			t.Errorf("Asking for %s, should have yielded %s", k, v)
		}
	}

	// Adds 8, 18, 28
	hash.Add("8")

	// 27 should now map to 8.
	testCases["27"] = "8"

	for k, v := range testCases {
		if hash.Get(k) != v {
			t.Errorf("Asking for %s, should have yielded %s", k, v)
		}
	}

}
