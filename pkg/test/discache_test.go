package test

import (
	"distributed-cache/pkg/src"
	"fmt"
	"log"
	"testing"
)

var db = map[string]string{
	"Tom":  "630",
	"Jack": "589",
	"Sam":  "567",
}

func TestDisCache(t *testing.T) {
	loadCounts := make(map[string]int, len(db))
	gee := disCache.NewGroup("scores", 2<<4, disCache.GetterFunc(
		func(key string) (string, error) {
			log.Println("[SlowDB] search key", key)
			if v, ok := db[key]; ok {
				if _, ok = loadCounts[key]; ok {
					loadCounts[key] += 1
				}
				return v, nil
			}
			return "", fmt.Errorf("%s not exist", key)
		}))

	for k, v := range db {
		if view, err := gee.Get(k); err != nil || view != v {
			t.Fatal("failed to get value of Tom")
		} // load from callback function
		if _, err := gee.Get(k); err != nil || loadCounts[k] > 1 {
			t.Fatalf("cache %s miss", k)
		} // cache hit
	}

	if view, err := gee.Get("unknown"); err == nil {
		t.Fatalf("the value of unknow should be empty, but %s got", view)
	}
}
