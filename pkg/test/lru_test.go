package test

import (
	"distributed-cache/pkg/src"
	"testing"
)

type node struct {
	operation string
	key       string
	value     string
}

func TestGet(t *testing.T) {
	tests := []struct {
		name      string
		operation []string
		arg       []node
		want      []string
	}{{
		name: "case1",
		arg: []node{
			{"add", "111", "111"},
			{"add", "222", "222"},
			{"add", "333", "333"},
			{"add", "444", "444"},
			{"add", "555", "555"},
		},
		want: []string{"555", "444", "333", "222"},
	}, {
		name: "case2",
		arg: []node{
			{"add", "111", "111"},
			{"add", "222", "222"},
			{"add", "333", "333"},
			{"get", "111", "111"},
			{"add", "555", "555"},
		},
		want: []string{"555", "111", "333", "222"},
	}}

	for _, tt := range tests {
		cacheLRU := disCache.NewCacheLRU(100)
		t.Run(tt.name, func(t *testing.T) {
			for i := range tt.arg {
				switch tt.arg[i].operation {
				case "add":
					cacheLRU.Add(tt.arg[i].key, tt.arg[i].value)
				case "delete":
					cacheLRU.Delete(tt.arg[i].key)
				case "get":
					cacheLRU.Get(tt.arg[i].key)
				}
			}
			res := cacheLRU.Print()
			for i := 0; i < len(tt.want); i++ {
				if res[i] != tt.want[i] {
					t.Error("TestGet error")
					return
				}
			}
		})
	}
}
